﻿using System;
using System.Collections.Generic;
using BEZAO___Task_3.Helpers;
using BEZAO___Task_3.Interfaces;
using BEZAO___Task_3.Services;

namespace BEZAO___Task_3.Models
{
    public class Customer
    {
        public int Id { get; set; }
        private string LastName { get; set; }
        private string FirstName { get; set; }
        public string FullName { get; set; }
        public double Balance { get; set; }
        public long AccountNumber { get; set; }
        public DateTime DateOfBirth { get; set; }

        public DateTime CreatedOn { get; set; }
        public IList<Transaction> Transactions { get; set; }

        private readonly ILogger _logger;

        public Customer()
        {
            _logger = new Logger();
        }
        public Customer( string firstName, string lastName, DateTime dateOfBirth)
        {
            Transactions = new List<Transaction>();
            FirstName = firstName;
            LastName = lastName;
            FullName = $"{FirstName} {LastName}";
            Balance = 0.00;
            DateOfBirth = dateOfBirth;
            AccountNumber = long.Parse(Generator.GenerateAccountNumber);
            CreatedOn = DateTime.Now;
            
        }

        public void Deposit(int amount)
        {
            Balance += amount;
        }

        public void Withdraw(int amount)
        {
            if (amount >  Balance)
            {
               _logger.Log("Insufficient Fund");
            }

            Balance -= amount;
        }

        public void CheckBalance()
        {

        }
    }
}
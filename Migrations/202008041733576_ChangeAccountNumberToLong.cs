﻿namespace BEZAO___Task_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeAccountNumberToLong : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Customers", "AccountNumber", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Customers", "AccountNumber", c => c.Int(nullable: false));
        }
    }
}

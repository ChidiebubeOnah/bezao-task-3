﻿namespace BEZAO___Task_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCreatedOnColumnToCustomersTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "CreatedOn", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "CreatedOn");
        }
    }
}

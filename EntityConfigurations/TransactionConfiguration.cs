﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEZAO___Task_3.Models;

namespace BEZAO___Task_3.EntityConfigurations
{
    public class TransactionConfiguration: EntityTypeConfiguration<Transaction>
    {
        public TransactionConfiguration()
        {

                Property(t => t.Remark)
                .HasMaxLength(1000);

                Property(t => t.TransactionType)
                .IsRequired();
        }
    }
}

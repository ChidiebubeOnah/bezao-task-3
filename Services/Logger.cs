﻿using System;
using BEZAO___Task_3.Interfaces;

namespace BEZAO___Task_3.Services
{
    public class Logger: ILogger
    {
        public void Log(string text)
        {
            Console.WriteLine(text);
        }
    }
}
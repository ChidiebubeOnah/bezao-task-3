﻿using System;
using System.Linq;
using System.Text;
using BEZAO___Task_3.Helpers;
using BEZAO___Task_3.Interfaces;
using BEZAO___Task_3.Models;
using BEZAO___Task_3.Persistence;

namespace BEZAO___Task_3.Services
{
    public class BankActions
    {
        private readonly ILogger _logger;
        private Func<string, string> _prompt;
        private readonly CustomerRepository _customerRepository;
        private readonly TransactionRepository _transactionRepository;
       


        public BankActions(ILogger logger)
        {
            _logger = logger;
            _customerRepository = new CustomerRepository(logger);
            _transactionRepository = new TransactionRepository();
        }
        public void CreateAccount()
        {
            Customer newCustomer = Form.CustomerEnrollment(_logger, _prompt);

           _prompt = TextBuilder.Prompt;

           _customerRepository.Add(newCustomer);

           _logger.Log(_prompt($"Congratulations!!!" +
                               $" {newCustomer.FullName}," +
                               $" Your Have Successful Created An Account With us\nYour Account Number is: " +
                               $"{newCustomer.AccountNumber}\nYour Current Account Balance is: NGN{newCustomer.Balance:N}"));
            
        }

        public void CheckBalance()
        {
            long accountNumber = Form.Account(_logger, _prompt);
            var customerBalance = _customerRepository.GetCustomer(accountNumber);
            _prompt = TextBuilder.Prompt;
            if (customerBalance != null)
            {
                _logger.Log(_prompt($"Dear, {customerBalance.FullName} Your Account Balance is: NGN{customerBalance.Balance:N}"));
            }
            else
            {
                _logger.Log(_prompt("This account Number does not exist in our Database!"));
            }

            
        }

        public void Deposit()
        {
            var transaction = Form.Transaction(_logger, _prompt);
            _customerRepository.BankAlert += _transactionRepository.AddCredit;
            _customerRepository.CustomerDeposit(transaction.account, transaction.amount);

        }

        public void Withdraw()
        {
            var transaction = Form.Transaction(_logger, _prompt);
            _customerRepository.BankAlert += _transactionRepository.AddCredit;
            _customerRepository.CustomerWithdraw(transaction.account, transaction.amount);

        }

        public void GetCustomerTransactions()
        {
            long accountNumber = Form.Account(_logger, _prompt);
            var customerTransactions = _customerRepository.GetCustomer(accountNumber).Transactions.ToList();
            _prompt = TextBuilder.Prompt;
            var sb = new StringBuilder();

            foreach (var transaction in customerTransactions)
            {
               
                _logger.Log(_prompt($"Time:{transaction.Time}\nRemark:\n{ transaction.Remark}"));

            }


        }

        public void GetAllTransactions()
        {
            var allCustomersTransactions = _customerRepository.GetCustomers();
            _prompt = TextBuilder.Prompt;

            foreach (var customer in allCustomersTransactions)
            {

                _logger.Log($"\nName:{customer.FullName}\nAccount:\n{ customer.AccountNumber}");
                if (customer.Transactions.Count == 0)
                {
                    _logger.Log(_prompt($"No Transaction History"));
                }
                else
                {
                    foreach (var transaction in customer.Transactions)
                    {




                        _logger.Log(_prompt($"Time:{transaction.Time}\nRemark:\n{ transaction.Remark}"));
                    }

                }

            }


        }

    }
}